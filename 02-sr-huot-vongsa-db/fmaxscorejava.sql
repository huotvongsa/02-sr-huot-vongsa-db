DROP FUNCTION passed_exam_students();

CREATE FUNCTION passed_exam_students()
RETURNS TABLE(
	id SMALLINT,
	name VARCHAR,
	gender VARCHAR,
	Class_Name VARCHAR,
	subject VARCHAR,
	score FLOAT,
	exam_date DATE
	)
	
	AS 
	$$
		BEGIN
			RETURN QUERY SELECT 
		 s.id,
		 s.name,
		 s.gender,
		 cl.name AS class_name,
		 su.name AS Subject,
		 CAST(ed.score AS FLOAT),
		 CAST(ed.exam_date AS DATE)
				FROM
						student s
								INNER JOIN exam_detail ed
													 ON ed.student_id=s.id
								INNER JOIN classroom CL
													 On cl.id=s.id
								INNER JOIN subject su
													 ON su.id =ed.subject_id
				WHERE subject_id=1 and ed.exam_date BETWEEN '2021-04-01' and '2021-06-12'
							ORDER BY ed.score DESC
							LIMIT 1   	
		END
	$$
		LANGUAGE plpgsql;
		
		SELECT * FROM passed_exam_students();